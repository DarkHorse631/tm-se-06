package ru.grishin.tm.service;

import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.repository.UserRepository;
import ru.grishin.tm.util.HashUtil;

import java.util.Collection;
import java.util.UUID;

public class UserServiceImpl {
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User login(String login, String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        User user = userRepository.findLogin(login);
        if (user.getPassword().equals(HashUtil.passwordToHash(password))) return user;
        return null;
    }

    public void registryUser(String login, String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findLogin(login) != null) return;
        userRepository.persist(new User(UUID.randomUUID().toString(), login, HashUtil.passwordToHash(password), RoleType.USER));
    }

    public void registryAdmin(String login, String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        userRepository.persist(new User(UUID.randomUUID().toString(), login, HashUtil.passwordToHash(password), RoleType.ADMIN));
    }

    public void update(String id, String login, String password, RoleType roleType) {
        if (id == null || id.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        userRepository.update(id, login, password, roleType);
    }

    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        userRepository.remove(id);
    }

    public User findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public void merge(User user) {
        if (user.getId() == null || user.getId().isEmpty()) return;
        userRepository.merge(user);
    }

    public void updatePassword(String id, String currentPassword, String newPassword) {
        if (id == null || id.isEmpty()) return;
        if (currentPassword == null || currentPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (userRepository.findOne(id) == null) return;
        if (!HashUtil.passwordToHash(currentPassword).equals(userRepository.findOne(id).getPassword())) return;
        userRepository.updatePassword(id, HashUtil.passwordToHash(newPassword));
    }
}
