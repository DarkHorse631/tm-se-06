package ru.grishin.tm.service;

import ru.grishin.tm.api.ProjectService;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String userId, String name, String description, Date dateStart, Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.persist(new Project(UUID.randomUUID().toString(), userId, name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(String userId, String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        projectRepository.remove(userId, id);
    }

    @Override
    public void update(String userId, String id, String name, String description, Date dateStart, Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public Project findOne(String userId, String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

    @Override
    public void merge(Project project) {
        if (project.getId() == null || project.getId().isEmpty()) return;
        projectRepository.merge(project);
    }

    @Override
    public Collection<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

}
