package ru.grishin.tm.service;

import ru.grishin.tm.api.TaskService;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String projectId, String userId, String name, String description, Date dateStart, Date dateFinish) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        taskRepository.persist(new Task(projectId, userId, UUID.randomUUID().toString(), name, description, dateStart, dateFinish));
    }

    @Override
    public void remove(String userId, String id) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        taskRepository.remove(userId, id);
    }

    @Override
    public void update(String userId, String id, String name, String description, Date dateStart, Date dateFinish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        taskRepository.update(userId, id, name, description, dateStart, dateFinish);
    }

    @Override
    public void deleteByProjectId(String userId, String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.deleteByProjectId(userId, projectId);
    }


    @Override
    public Task findOne(String userId, String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    @Override
    public Collection<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public void merge(Task task) {
        if (task.getId() == null || task.getId().isEmpty()) return;
        taskRepository.merge(task);
    }

}
