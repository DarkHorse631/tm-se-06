package ru.grishin.tm.api;

import ru.grishin.tm.entity.Task;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

public interface TaskService {

    void create(String projectId, String userId, String taskName, String description, Date startDate, Date completionDate);

    void remove(String userId, String id);

    void update(String userId, String id, String taskName, String description, Date startDate, Date completionDate);

    Task findOne(String userId, String id);

    Collection<Task> findAll(String userId);

    void merge(Task task);

    void deleteByProjectId(String userId, String projectId);
}
