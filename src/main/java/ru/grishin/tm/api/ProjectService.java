package ru.grishin.tm.api;

import ru.grishin.tm.entity.Project;

import java.util.Collection;
import java.util.Date;

public interface ProjectService {

    void create(String userId, String projectName, String description, Date startDate, Date completionDate);

    void remove(String userId, String id);

    void update(String userId, String id, String projectName, String description, Date startDate, Date completionDate);

    Project findOne(String userId, String id);

    Collection<Project> findAll(String userId);


    void merge(Project project);
}
