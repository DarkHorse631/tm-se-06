package ru.grishin.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String FORMAT_DATE(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String result = null;
        return result = dateFormat.format(date);
    }

    public static Date PARSE_DATE(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date result = null;
        try {
            result = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date is null");
        }
        return result;
    }
}
