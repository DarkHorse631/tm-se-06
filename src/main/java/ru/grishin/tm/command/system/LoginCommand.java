package ru.grishin.tm.command.system;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

public class LoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws Exception {
        User user;
        System.out.println("--Identification--");
        System.out.print("Enter login: ");
        String login = bootstrap.getScanner().nextLine();
        System.out.print("Enter password: ");
        String password = bootstrap.getScanner().nextLine();
        user = bootstrap.getUserService().login(login, password);
        bootstrap.setCurrentUser(user);
        if(user == null) System.out.println("Identification failed");
        else System.out.println("[IDENTIFICATION COMPLETE]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER};
    }
}
