package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class ProjectShowAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "psa";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Show all projects--");
        for (Object o : bootstrap.getProjectService().findAll(bootstrap.getCurrentUser().getId()))
            System.out.println(o);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
