package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "pcl";
    }

    @Override
    public String getDescription() {
        return "Clear the Project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Clear Project--");
        System.out.print("Enter project id: ");
        String projectId = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().deleteByProjectId(bootstrap.getCurrentUser().getId(), projectId);
        System.out.println("[PROJECT CLEAR]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
