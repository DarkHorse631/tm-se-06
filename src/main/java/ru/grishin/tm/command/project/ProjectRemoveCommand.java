package ru.grishin.tm.command.project;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "pr";
    }

    @Override
    public String getDescription() {
        return "Remove project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Remove project--");
        System.out.print("Enter the project id: ");
        String projectId = bootstrap.getScanner().nextLine();
        bootstrap.getProjectService().remove(bootstrap.getCurrentUser().getId(), projectId);
        bootstrap.getTaskService().deleteByProjectId(bootstrap.getCurrentUser().getId(), projectId);
        System.out.println("[PROJECT DELETED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
