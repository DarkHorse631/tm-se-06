package ru.grishin.tm.command.user;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class UserCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ur";
    }

    @Override
    public String getDescription() {
        return "Register a new user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Create new user--");
        System.out.print("Enter your login: ");
        String login = bootstrap.getScanner().nextLine();
        System.out.print("Enter your password: ");
        String pass1 = bootstrap.getScanner().nextLine();
        System.out.print("Confirm your password: ");
        String pass2 = bootstrap.getScanner().nextLine();
        if (pass1.equals(pass2)) {
            bootstrap.getUserService().registryUser(login, pass1);
            System.out.println("[USER CREATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }

}
