package ru.grishin.tm.command.user;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "uup";
    }

    @Override
    public String getDescription() {
        return "Update password to current user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Update current password--");
        System.out.print("Enter current password: ");
        String currentPassword = bootstrap.getScanner().nextLine();
        System.out.print("Enter new password: ");
        String pass1 = bootstrap.getScanner().nextLine();
        System.out.print("Confirm your password: ");
        String pass2 = bootstrap.getScanner().nextLine();
        if (pass1.equals(pass2)) {
            bootstrap.getUserService().updatePassword(bootstrap.getCurrentUser().getId(), currentPassword, pass1);
            System.out.println("[PASSWORD UPDATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
