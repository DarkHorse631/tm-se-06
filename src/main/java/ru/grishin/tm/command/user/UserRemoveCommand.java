package ru.grishin.tm.command.user;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class UserRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "ud";
    }

    @Override
    public String getDescription() {
        return "Delete user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--DELETE USER--");
        System.out.print("Enter user id: ");
        String id = bootstrap.getScanner().nextLine();
        bootstrap.getUserService().remove(id);
        System.out.println("[USER [" + id + "] DELETED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN};
    }
}
