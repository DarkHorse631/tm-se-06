package ru.grishin.tm.command.user;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class UserUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "uu";
    }

    @Override
    public String getDescription() {
        return "Update current user.";
    }

    @Override
    public void execute() throws Exception {

        System.out.println("--Update current user--");
        System.out.print("Enter new login: ");
        String login = bootstrap.getScanner().nextLine();
        System.out.print("Enter new password: ");
        String pass1 = bootstrap.getScanner().nextLine();
        System.out.print("Confirm your password: ");
        String pass2 = bootstrap.getScanner().nextLine();
        if (pass1.equals(pass2)) {
            bootstrap.getUserService().update(bootstrap.getCurrentUser().getId(), login, pass1, RoleType.USER);
            System.out.println("[USER UPDATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
