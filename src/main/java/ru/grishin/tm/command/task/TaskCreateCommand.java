package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tc";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Create task into project--");
        System.out.print("Enter project id: ");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.print("Enter task name: ");
        String name = bootstrap.getScanner().nextLine();
        System.out.print("Enter task description: ");
        String description = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().create(projectId, bootstrap.getCurrentUser().getId(), name, description, new Date(), new Date());
        System.out.println("[TASK CREATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
