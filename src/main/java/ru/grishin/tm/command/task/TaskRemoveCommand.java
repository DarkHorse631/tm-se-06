package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "td";
    }

    @Override
    public String getDescription() {
        return "Remove task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Remove task from project--");
        System.out.print("Enter task id: ");
        String taskId = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().remove(bootstrap.getCurrentUser().getId(),taskId);
        System.out.println("[TASK DELETED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER,RoleType.ADMIN};
    }
}
