package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tu";
    }

    @Override
    public String getDescription() {
        return "Update task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Update task--");
        System.out.print("Enter task id:");
        String taskId = bootstrap.getScanner().nextLine();
        System.out.print("Enter new name:");
        String name = bootstrap.getScanner().nextLine();
        System.out.print("Enter new description:");
        String description = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().update(bootstrap.getCurrentUser().getId(), taskId, name, description, new Date(), new Date());
        System.out.println("[TASK UPDATED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
