package ru.grishin.tm.command.task;

import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Date;

public class TaskMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "tm";
    }

    @Override
    public String getDescription() {
        return "Merge task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("--Merge task--");
        System.out.print("Enter project id: ");
        String projectId = bootstrap.getScanner().nextLine();
        System.out.print("Enter task id: ");
        String id = bootstrap.getScanner().nextLine();
        System.out.print("Enter task name: ");
        String name = bootstrap.getScanner().nextLine();
        System.out.print("Enter task description: ");
        String description = bootstrap.getScanner().nextLine();
        bootstrap.getTaskService().merge(new Task(projectId, bootstrap.getCurrentUser().getId(), id, name, description, new Date(), new Date()));
        System.out.println("[TASK MERGED]");
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
