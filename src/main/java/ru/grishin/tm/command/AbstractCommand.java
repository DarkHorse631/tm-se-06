package ru.grishin.tm.command;

import ru.grishin.tm.bootstrap.Bootstrap;
import ru.grishin.tm.enumerate.RoleType;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract RoleType [] roles();

}
