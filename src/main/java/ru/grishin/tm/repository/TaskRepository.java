package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    public void persist(Task task) {
        tasks.put(task.getId(), task);
    }

    public void insert(String projectId, String userId, String id, String name, String description, Date dateStart, Date dateFinish) {
        tasks.put(id, new Task(projectId, userId, id, name, description, dateStart, dateFinish));
    }

    public void remove(String userId, String id) {
        Task task = tasks.get(id);
        if (task == null) return;
        if (task.getUserId().equals(userId)) tasks.remove(id);
    }

    public Task findOne(String userId, String id) {
        Task task = tasks.get(id);
        if (task == null) return null;
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    public void deleteByProjectId(String userId, String projectId) {
        Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getProjectId().equals(projectId)) entryIterator.remove();
        }
    }

    public Collection<Task> findAll(String userId) {
        Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = tasks.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    public void merge(Task task) {
        if (tasks.containsKey(task.getId()))
            update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
        else
            insert(task.getProjectId(), task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateStart(), task.getDateFinish());
    }

    public void mergeAll(Task... tasks) {
        for (Task task : tasks) {
            merge(task);
        }
    }

    public void update(String userId, String id, String name, String description, Date dateStart, Date dateFinish) {
        Task task = tasks.get(id);
        if (!task.getUserId().equals(userId)) return;
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        tasks.put(task.getId(), task);
    }

    public void removeAll() {
        tasks.clear();
    }

}
