package ru.grishin.tm.repository;

import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.entity.User;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

public class UserRepository {

    private Map<String, User> users = new LinkedHashMap<>();

    public void persist(User user) {
        users.put(user.getId(), user);
    }

    public void insert(String id, String login, String password, RoleType roleType) {
        users.put(id, new User(id, login, password, roleType));
    }

    public void update(String id, String login, String password, RoleType roleType) {
        User user = users.get(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setRoleType(roleType);
        users.put(user.getId(), user);
    }

    public void remove(String id) {
        users.remove(id);
    }

    public void merge(User user) {
        if (users.containsKey(user.getId()))
            update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        else
            insert(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
    }

    public void updatePassword(String id, String password) {
        users.get(id).setPassword(password);
    }

    public User findOne(String id) {
        return users.get(id);
    }

    public User findLogin(String login) {
        for (User user : users.values()) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public Collection<User> findAll() {
        Collection<User> output = new LinkedList<>();
        return output = users.values();
    }

}
