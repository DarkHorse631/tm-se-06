package ru.grishin.tm.repository;

import ru.grishin.tm.entity.Project;

import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    public void insert(String id, String userId, String name, String description, Date startDate, Date completionDate) {
        projects.put(id, new Project(id, userId, name, description, startDate, completionDate));
    }

    public void merge(Project project) {
        if (projects.containsKey(project.getId()))
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
        else
            insert(project.getId(), project.getUserId(), project.getName(), project.getDescription(), project.getDateStart(), project.getDateFinish());
    }

    public void mergeAll(Project... projects) {
        for (Project project : projects)
            merge(project);
    }

    public Project findOne(String userId, String id) {
        Project project = projects.get(id);
        if (project == null) return null;
        if (project.getUserId().equals(userId)) return project;
        return null;
    }

    public void remove(String userId, String id) {
        Project project = projects.get(id);
        if (project == null) return;
        if (project.getUserId().equals(userId)) projects.remove(id);
    }

    public Collection<Project> findAll(final String userId) {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = projects.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        return result;
    }

    public void update(String userId, String id, String name, String description, Date startDate, Date completionDate) {
        Project project = projects.get(id);
        if (!project.getUserId().equals(userId)) return;
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(startDate);
        project.setDateFinish(completionDate);
        projects.put(id, project);
    }

    public void clearProjectList() {
        projects.clear();
    }

}
