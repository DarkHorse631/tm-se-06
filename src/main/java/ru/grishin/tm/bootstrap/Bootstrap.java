package ru.grishin.tm.bootstrap;

import ru.grishin.tm.api.ProjectService;
import ru.grishin.tm.api.TaskService;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.repository.ProjectRepository;
import ru.grishin.tm.repository.TaskRepository;
import ru.grishin.tm.repository.UserRepository;
import ru.grishin.tm.service.ProjectServiceImpl;
import ru.grishin.tm.service.TaskServiceImpl;
import ru.grishin.tm.service.UserServiceImpl;

import java.util.*;

public class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskServiceImpl(taskRepository);
    private final UserRepository userRepository = new UserRepository();
    private final UserServiceImpl userService = new UserServiceImpl(userRepository);
    private final Scanner scanner = new Scanner(System.in);
    private User currentUser = null;

    {
        userService.registryUser("user", "user");
        userService.registryAdmin("admin", "admin");
    }

    public void init(Class... classes) throws Exception {
        if (classes == null) return;
        for (Class clazz : classes)
            registry(clazz);
    }

    public void registry(Class clazz) throws Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        AbstractCommand command = (AbstractCommand) clazz.newInstance();
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setBootstrap(this);
        commands.put(cliCommand, command);

    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            System.out.print("--Input:");
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (roleCheck(abstractCommand)) abstractCommand.execute();
    }

    private boolean roleCheck(final AbstractCommand command) {
        if (currentUser == null && Arrays.asList(command.roles()).contains(RoleType.ANONYMOUS_USER)) return true;
        if (currentUser == null) return false;
        for (RoleType role : command.roles()) {
            if (role == currentUser.getRoleType()) {
                return true;
            }
        }
        System.out.println("Command is not available!");
        return false;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserServiceImpl getUserService() {
        return userService;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Collection<AbstractCommand> getCommands() {
        return new LinkedList<AbstractCommand>(commands.values());
    }

}
